package com.webinfotech.tambolanumbers.domain.Model;

import android.widget.TextView;

public class NumberData {
    public int number;
    public boolean isSelected = false;

    public NumberData(int txt_view_TambolaNumbers, boolean isSelected) {
        this.number = txt_view_TambolaNumbers;
        this.isSelected = isSelected;
    }

}
