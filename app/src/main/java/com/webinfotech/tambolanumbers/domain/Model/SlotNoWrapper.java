package com.webinfotech.tambolanumbers.domain.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SlotNoWrapper {

    @SerializedName("status")
    @Expose
    public boolean status;

    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("data")
    @Expose
    public SlotNo[] slotNos;

}
