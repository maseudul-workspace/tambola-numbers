package com.webinfotech.tambolanumbers.domain.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SlotNo {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("slot_no")
    @Expose
    public int slotNo;

    @SerializedName("number")
    @Expose
    public int number;

}
