package com.webinfotech.tambolanumbers.domain.interactors.impl;

import com.webinfotech.tambolanumbers.domain.Model.SlotNo;
import com.webinfotech.tambolanumbers.domain.Model.SlotNoWrapper;
import com.webinfotech.tambolanumbers.domain.executors.Executor;
import com.webinfotech.tambolanumbers.domain.executors.MainThread;
import com.webinfotech.tambolanumbers.domain.interactors.GetSlotNoInteractor;
import com.webinfotech.tambolanumbers.domain.interactors.base.AbstractInteractor;
import com.webinfotech.tambolanumbers.repository.AppRepositoryImpl;

public class GetSlotNoInteractorImpl extends AbstractInteractor implements GetSlotNoInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;

    public GetSlotNoInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingSlotNoFail(errorMsg);
            }
        });
    }

    private void postMessage(SlotNo[] slotNos){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingSlotNoSuccess(slotNos);
            }
        });
    }

    @Override
    public void run() {
        final SlotNoWrapper slotNoWrapper = mRepository.fetchSlotNoWrapper();
        if (slotNoWrapper == null) {
            notifyError("Something Went Wrong");
        } else if (!slotNoWrapper.status) {
            notifyError(slotNoWrapper.message);
        } else {
            postMessage(slotNoWrapper.slotNos);
        }
    }
}
