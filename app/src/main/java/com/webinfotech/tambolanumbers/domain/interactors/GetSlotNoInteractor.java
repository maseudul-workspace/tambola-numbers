package com.webinfotech.tambolanumbers.domain.interactors;

import com.webinfotech.tambolanumbers.domain.Model.SlotNo;

public interface GetSlotNoInteractor {
    interface Callback {
        void onGettingSlotNoSuccess(SlotNo[] slotNos);
        void onGettingSlotNoFail(String errorMsg);
    }
}
