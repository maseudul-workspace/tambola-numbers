package com.webinfotech.tambolanumbers.presentation.presenters.impl;

import com.webinfotech.tambolanumbers.domain.Model.SlotNo;
import com.webinfotech.tambolanumbers.domain.executors.Executor;
import com.webinfotech.tambolanumbers.domain.executors.MainThread;
import com.webinfotech.tambolanumbers.domain.interactors.GetSlotNoInteractor;
import com.webinfotech.tambolanumbers.domain.interactors.impl.GetSlotNoInteractorImpl;
import com.webinfotech.tambolanumbers.presentation.presenters.GetSlotNoPresenter;
import com.webinfotech.tambolanumbers.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.tambolanumbers.repository.AppRepositoryImpl;

public class GetSlotNoPresenterImpl extends AbstractPresenter implements GetSlotNoPresenter, GetSlotNoInteractor.Callback {

    GetSlotNoPresenter.View mView;
    GetSlotNoInteractorImpl getSlotNoInteractor;

    public GetSlotNoPresenterImpl(Executor executor, MainThread mainThread, GetSlotNoPresenter.View view) {
        super(executor, mainThread);
        this.mView = view;
    }


    @Override
    public void fetchSlotNos() {
        getSlotNoInteractor = new GetSlotNoInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this);
        getSlotNoInteractor.execute();
    }

    @Override
    public void onGettingSlotNoSuccess(SlotNo[] slotNos) {
        mView.loadSlotNos(slotNos);
    }

    @Override
    public void onGettingSlotNoFail(String errorMsg) {

    }
}
