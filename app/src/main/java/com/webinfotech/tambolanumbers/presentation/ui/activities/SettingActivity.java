package com.webinfotech.tambolanumbers.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.webinfotech.tambolanumbers.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SettingActivity extends AppCompatActivity {
    @BindView(R.id.txt_view_setting_start)
    TextView SettingStart;
    @BindView(R.id.radio_group_setting)
    RadioGroup radioGroupSetting;
    boolean isManual = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        ButterKnife.bind(this);
        setRadioGroupSetting();
    }

    public void setRadioGroupSetting() {
        radioGroupSetting.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radio_manual:
                        isManual = true;
                        break;
                    case R.id.radio_automatic:
                        isManual = false;
                        break;
                }
            }
        });
    }

    @OnClick(R.id.txt_view_setting_start) void StartingGame()
    {
        Intent intent = new Intent(this, StartGameActivity.class);
        intent.putExtra("isManual", isManual);
        startActivity(intent);
        finish();
    }
}
