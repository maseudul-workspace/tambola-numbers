package com.webinfotech.tambolanumbers.presentation.ui.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.webinfotech.tambolanumbers.R;
import com.webinfotech.tambolanumbers.domain.Model.NumberData;

import org.w3c.dom.Text;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Tambola_new_game_reycler extends RecyclerView.Adapter<Tambola_new_game_reycler.MyViewHolder> {

    Context context;
    List<NumberData> numberList;

    public Tambola_new_game_reycler(Context context, List<NumberData> numberList) {
        this.context = context;
        this.numberList = numberList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        LayoutInflater layoutInflater=LayoutInflater.from(context);
        view=layoutInflater.inflate(R.layout.new_start_layout_items,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        if (numberList.get(position).isSelected) {
            holder.layoutSelected.setVisibility(View.VISIBLE);
            holder.roundView.setVisibility(View.GONE);
        } else {
            holder.layoutSelected.setVisibility(View.GONE);
            holder.roundView.setVisibility(View.VISIBLE);
        }
        if (numberList.get(position).number < 10) {
            holder.txtViewCurrentScore.setText("0" + numberList.get(position).number);
        } else {
            holder.txtViewCurrentScore.setText(Integer.toString(numberList.get(position).number));
        }    }

    @Override
    public int getItemCount() {
        return numberList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_round_border)
        View roundView;
        @BindView(R.id.txt_view_current_score)
        TextView txtViewCurrentScore;
        @BindView(R.id.layout_selected)
        View layoutSelected;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);

        }
    }

    public void updateDataSet(List<NumberData> numberDataList) {
        this.numberList = numberDataList;
        notifyDataSetChanged();
    }

}
