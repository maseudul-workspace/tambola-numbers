package com.webinfotech.tambolanumbers.presentation.ui.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.webinfotech.tambolanumbers.R;
import com.webinfotech.tambolanumbers.domain.Model.NumberData;
import com.webinfotech.tambolanumbers.domain.Model.SlotNo;
import com.webinfotech.tambolanumbers.domain.executors.impl.ThreadExecutor;
import com.webinfotech.tambolanumbers.presentation.presenters.GetSlotNoPresenter;
import com.webinfotech.tambolanumbers.presentation.presenters.impl.GetSlotNoPresenterImpl;
import com.webinfotech.tambolanumbers.presentation.ui.adapters.NumberListAdapters;
import com.webinfotech.tambolanumbers.presentation.ui.adapters.Tambola_start_recyclerView;
import com.webinfotech.tambolanumbers.presentation.ui.dialogs.NumberListDialog;
import com.webinfotech.tambolanumbers.threading.MainThreadImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.Timer;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class StartGameActivity extends AppCompatActivity implements GetSlotNoPresenter.View {

    @BindView(R.id.txt_view_mode)
    TextView txtViewMode;
    @BindView(R.id.txt_view_manual_mode)
    TextView txtViewManual;
    @BindView(R.id.txt_view_automatic_mode)
    TextView txtViewAutomatic;
    @BindView(R.id.txt_view_last_number)
    TextView txtViewlastNumber;
    @BindView(R.id.txt_view_list)
    TextView txtViewNumberList;
    @BindView(R.id.txt_view_start_game)
    TextView txtViewStartGame;
    @BindView(R.id.tambula_number_ryc)
    RecyclerView tambula_numberRyc;
    @BindView(R.id.speaker_on)
    ImageView speakerOnImg;
    @BindView(R.id.speaker_off)
     ImageView speakerOffImg;
    @BindView(R.id.img_next_arrow)
    ImageView nxtNumber;
    AlertDialog.Builder builder;
    TextToSpeech textToSpeech;
    @BindView(R.id.play)
    ImageView playImg;
    @BindView(R.id.pause)
    ImageView pauseImg;
    @BindView(R.id.txt_view_resume_alert)
    TextView txtResumePlay;
    @BindView(R.id.delayTime_layout)
    LinearLayout DelayTimeLayout;
    @BindView(R.id.delay_radio_group)
    RadioGroup delayRadioGroup;
    @BindView(R.id.txt_view_current_score)
    TextView txtViewCurrentScore;
    Animation animation;
    Tambola_start_recyclerView tambola_start_recyclerViewAdapter;
    List<NumberData> tambola_starts_arrayList;
    int currentNumber = 0;
    List<Integer> previousIntegerArraylist = new ArrayList<>();
    Handler handler = new Handler();
    Runnable runnable;
    int interval = 5000;
    boolean isManual = true;
    NumberListDialog numberListDialog;
    NumberListAdapters numberListAdapters;
    boolean speakerSound=true;
    GetSlotNoPresenterImpl mPresenter;
    SlotNo[] slotNos;
    int slotCount = 1;
    private InterstitialAd mInterstitialAd;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_game);
        isManual = getIntent().getBooleanExtra("isManual", true);
        ButterKnife.bind(this);
        initialisePresenter();
        mPresenter.fetchSlotNos();
        builder=new AlertDialog.Builder(this);
        setTexttoSpeech();
        setArrayList();
        setTambolaRecyclerView();
        setDelayRadioGroup();
        initialiseNumberListDialog();
        // prepare interstitial ads

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-3940256099942544/1033173712");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());


    }



    public void initialisePresenter() {
        mPresenter = new GetSlotNoPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this);
    }

    private void setTexttoSpeech() {
        speakerSound=true;
        textToSpeech=new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int i) {
                if (i!=TextToSpeech.ERROR)
                {
                    textToSpeech.setLanguage(Locale.ENGLISH);
                }
            }
        });
    }

    public void initialiseNumberListDialog() {
        numberListDialog = new NumberListDialog(this, this);
        numberListDialog.setUpDialogView();
        numberListAdapters = new NumberListAdapters(previousIntegerArraylist);
        numberListDialog.setRecyclerView(numberListAdapters);
    }

    public void setTambolaRecyclerView() {
        tambula_numberRyc=(RecyclerView)findViewById(R.id.tambula_number_ryc);
        tambola_start_recyclerViewAdapter=new Tambola_start_recyclerView(this,tambola_starts_arrayList);
        tambula_numberRyc.setLayoutManager(new GridLayoutManager(this,10));
        tambula_numberRyc.setAdapter(tambola_start_recyclerViewAdapter);
    }

    private void setArrayList() {
        tambola_starts_arrayList=new ArrayList<>();
        for (int i=1;i<=90;i++)
        {
            tambola_starts_arrayList.add(new NumberData(i, false));
        }

    }

    private void setDelayRadioGroup() {
        delayRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId) {
                    case R.id.radio_btn_delay_4:
                        interval = 4000;
                        break;
                    case R.id.radio_btn_delay_5:
                        interval = 5000;
                        break;
                    case  R.id.radio_btn_delay_7:
                        interval = 7000;
                        break;
                }
            }
        });
    }

    @OnClick(R.id.img_next_arrow) void nextNumber()
    {
        if (previousIntegerArraylist.size() < 90) {
            setNumber();
            updateRecyclerView();
        }
    }

    void setNumber() {
        if (slotCount == 90) {
            handler.removeCallbacks(runnable);
            runnable = null;
            showEndGameDialog();
        } else {
            if (currentNumber > 0) {
                txtViewlastNumber.setText(Integer.toString(currentNumber));
            }
            if (checkCurrentSlotNo(slotCount)) {
                currentNumber = getSlotNoItem(slotCount);
                previousIntegerArraylist.add(currentNumber);
                numberListAdapters.setNewData(previousIntegerArraylist);
                txtViewCurrentScore.setText(Integer.toString(currentNumber));
                slotCount++;
                if (speakerSound)
                {
                    textToSpeech.speak(Integer.toString(currentNumber),TextToSpeech.QUEUE_FLUSH,null);
                }
            } else {
                Random random=new Random();
                int number = random.nextInt(90)+1;
                if (checkSameNumber(number) || checkSlotNo(number)) {
                    setNumber();
                }
                else {
                    currentNumber = number;
                    previousIntegerArraylist.add(currentNumber);
                    numberListAdapters.setNewData(previousIntegerArraylist);
                    slotCount++;
                    if (currentNumber < 10) {
                        txtViewCurrentScore.setText("0" + currentNumber);
                    } else {
                        txtViewCurrentScore.setText(Integer.toString(currentNumber));
                    }
                    if (speakerSound)
                    {
                        speakNumber();
                    }
                }
            }
        }
    }

    void updateRecyclerView() {
        for (int i = 0; i < tambola_starts_arrayList.size(); i++) {
            if (tambola_starts_arrayList.get(i).number == currentNumber) {
                tambola_starts_arrayList.get(i).isSelected = true;
                tambola_start_recyclerViewAdapter.updateDataSet(tambola_starts_arrayList);
                break;
            }
        }
    }

    boolean checkSameNumber(int number) {
        if (previousIntegerArraylist.contains(number)) {
            return true;
        } else {
            return false;
        }
    }

    @OnClick(R.id.speaker_on) void soundOn()
    {   speakerSound=false;
        speakerOffImg.setVisibility(View.VISIBLE);
        speakerOnImg.setVisibility(View.GONE);
    }

    @OnClick(R.id.speaker_off) void soundOff()
    {   speakerSound=true;
        speakerOffImg.setVisibility(View.GONE);
        speakerOnImg.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.txt_view_manual_mode)void modeChangetoAutomatic()
    {
        isManual = false;
        txtViewAutomatic.setVisibility(View.VISIBLE);
        txtViewManual.setVisibility(View.INVISIBLE);
//        playImg.setVisibility(View.INVISIBLE);
//        txtResumePlay.setVisibility(View.INVISIBLE);

        if (nxtNumber.getVisibility()==View.VISIBLE)
        {
            nxtNumber.setVisibility(View.INVISIBLE);
        }
        else {
            nxtNumber.setVisibility(View.VISIBLE);
        }

        if (playImg.getVisibility()==View.VISIBLE)
        {
            playImg.setVisibility(View.INVISIBLE);
        }
        else {
            playImg.setVisibility(View.VISIBLE);
        }

        if (txtResumePlay.getVisibility()==View.VISIBLE)
        {
            txtResumePlay.setVisibility(View.INVISIBLE);
        }
        else {
            txtResumePlay.setVisibility(View.VISIBLE);
        }


//        pauseImg.setVisibility(View.INVISIBLE);
//        DelayTimeLayout.setVisibility(View.VISIBLE);
//        txtResumePlay.setVisibility(View.INVISIBLE);
    }
    @OnClick(R.id.txt_view_automatic_mode)void modeChangetoManual()
    {
        isManual = true;
        txtViewAutomatic.setVisibility(View.INVISIBLE);
        txtViewManual.setVisibility(View.VISIBLE);
        nxtNumber.setVisibility(View.VISIBLE);
        playImg.setVisibility(View.INVISIBLE);
        DelayTimeLayout.setVisibility(View.INVISIBLE);
        pauseImg.setVisibility(View.INVISIBLE);
        txtResumePlay.setVisibility(View.INVISIBLE);
        txtResumePlay.clearAnimation();
        handler.removeCallbacks(runnable);
    }
    @OnClick(R.id.pause)void displayPlayButton()
    {
        playImg.setVisibility(View.VISIBLE);
        pauseImg.setVisibility(View.INVISIBLE);
        DelayTimeLayout.setVisibility(View.INVISIBLE);
        animation= AnimationUtils.loadAnimation(this,R.anim.blink_animation);
        txtResumePlay.startAnimation(animation);
        txtResumePlay.setVisibility(View.VISIBLE);
        handler.removeCallbacks(runnable);

    }
    @OnClick(R.id.play) void displayPauseButton()
    {
        playImg.setVisibility(View.INVISIBLE);
        pauseImg.setVisibility(View.VISIBLE);
        DelayTimeLayout.setVisibility(View.VISIBLE);
        txtResumePlay.setVisibility(View.GONE);
        txtResumePlay.clearAnimation();
        runnable = new Runnable() {
            @Override
            public void run() {
                nextNumber();
                handler.postDelayed(runnable, interval);
            }
        };
        handler.post(runnable);

    }

    @OnClick(R.id.txt_view_start_game) void onStartGameClicked() {
        txtViewStartGame.setVisibility(View.INVISIBLE);
        txtViewlastNumber.setVisibility(View.VISIBLE);
        txtViewCurrentScore.setVisibility(View.VISIBLE);
        speakerOnImg.setVisibility(View.VISIBLE);
        txtViewMode.setVisibility(View.VISIBLE);
        txtViewNumberList.setVisibility(View.VISIBLE);
        if (isManual) {
            txtViewManual.setVisibility(View.VISIBLE);
            nxtNumber.setVisibility(View.VISIBLE);
            setNumber();
            updateRecyclerView();
        } else {
            txtViewAutomatic.setVisibility(View.VISIBLE);
            displayPauseButton();
        }
    }

    @OnClick(R.id.txt_view_list) void onTxtViewListClicked() {
        numberListDialog.showDialog();
        onPause();
    }

    @Override
    public void onBackPressed() {
        builder.setMessage("Do you want to leave the game?");
        builder.setNegativeButton("CONTINUE GAME", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                dialogInterface.dismiss();
            }
        });
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (mInterstitialAd.isLoaded())
                {
                    mInterstitialAd.show();
                    finish();
                }
                else {
                    mInterstitialAd.show();
                    finish();
                }
//                startActivity(new Intent(StartGameActivity.this,MainActivity.class));
            }
        });
        builder.show();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (!isManual) {
            handler.removeCallbacks(runnable);
            playImg.setVisibility(View.VISIBLE);
            pauseImg.setVisibility(View.INVISIBLE);
            txtResumePlay.setVisibility(View.VISIBLE);
            DelayTimeLayout.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void loadSlotNos(SlotNo[] slotNos) {
        Log.e("LogMsg", "Slot Count: " + slotNos.length);
        this.slotNos = slotNos;
    }

    public boolean checkSlotNo(int number) {
        try {
            for (int i = 0; i < slotNos.length; i++) {
                if (slotNos[i].number == number) {
                    return true;
                }
            }
            return false;
        } catch (NullPointerException e) {
            return false;
        }
    }

    public boolean checkCurrentSlotNo(int slotNo) {
        try {
            for (int i = 0; i < slotNos.length; i++) {
                if (slotNos[i].slotNo == slotNo) {
                    return true;
                }
            }
            return false;
        } catch (NullPointerException e) {
            return false;
        }
    }

    public int getSlotNoItem(int slotNo) {
        for (int i = 0; i < slotNos.length; i++) {
            if (slotNos[i].slotNo == slotNo) {
                return slotNos[i].number;
            }
        }
        return 0;
    }

    public void showEndGameDialog() {
        builder.setMessage("Hope you Enjoy Playing the Game");
        builder.setNegativeButton("GO BACK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        builder.setPositiveButton("RATE US!", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        builder.show();
        builder.setCancelable(false);
    }

    public void speakNumber() {
        textToSpeech.speak("",TextToSpeech.QUEUE_FLUSH,null);
        if (currentNumber < 10) {
            textToSpeech.speak("Single Number",TextToSpeech.QUEUE_ADD,null);
            textToSpeech.speak(Integer.toString(currentNumber),TextToSpeech.QUEUE_ADD,null);
        } else {
            String[] numberString = Integer.toString(currentNumber).split("");
            textToSpeech.speak(numberString[1],TextToSpeech.QUEUE_ADD,null);
            textToSpeech.speak(numberString[2],TextToSpeech.QUEUE_ADD,null);
            textToSpeech.speak(Integer.toString(currentNumber),TextToSpeech.QUEUE_ADD,null);
        }
    }

}
