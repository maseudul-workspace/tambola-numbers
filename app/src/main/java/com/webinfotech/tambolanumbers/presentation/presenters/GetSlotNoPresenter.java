package com.webinfotech.tambolanumbers.presentation.presenters;

import com.webinfotech.tambolanumbers.domain.Model.SlotNo;

public interface GetSlotNoPresenter {
    void fetchSlotNos();
    interface View {
        void loadSlotNos(SlotNo[] slotNos);
    }
}
