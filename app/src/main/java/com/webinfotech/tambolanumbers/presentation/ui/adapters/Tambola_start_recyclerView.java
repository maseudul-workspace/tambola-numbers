package com.webinfotech.tambolanumbers.presentation.ui.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.webinfotech.tambolanumbers.R;
import com.webinfotech.tambolanumbers.domain.Model.NumberData;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Tambola_start_recyclerView extends RecyclerView.Adapter<Tambola_start_recyclerView.ViewHolder>
{
    Context mContext;
    List<NumberData> numberDataList;


    public Tambola_start_recyclerView(Context mContext, List<NumberData> tambola) {
        this.mContext = mContext;
        numberDataList = tambola;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        LayoutInflater mInflater=LayoutInflater.from(mContext);
        view=mInflater.inflate(R.layout.layout_item_tambola_start,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (numberDataList.get(position).number < 10) {
            holder.tambolaStartText.setText("0" + numberDataList.get(position).number);
        } else {
            holder.tambolaStartText.setText(Integer.toString(numberDataList.get(position).number));
        }

        if (numberDataList.get(position).isSelected) {
            holder.tambolaStartText.setTextColor(mContext.getResources().getColor(R.color.lime_green));
        } else {
            holder.tambolaStartText.setTextColor(mContext.getResources().getColor(R.color.colorPrimaryDark));
        }

    }

    @Override
    public int getItemCount() {
        return numberDataList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tambola_numbers)
        TextView  tambolaStartText;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

    public void updateDataSet(List<NumberData> numberDataList) {
        Log.e("LogMsg", "Update data set");
        this.numberDataList = numberDataList;
        notifyDataSetChanged();
    }

}