package com.webinfotech.tambolanumbers.presentation.ui.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.preference.DialogPreference;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.warkiz.tickseekbar.OnSeekChangeListener;
import com.warkiz.tickseekbar.SeekParams;
import com.warkiz.tickseekbar.TickSeekBar;
import com.webinfotech.tambolanumbers.R;
import com.webinfotech.tambolanumbers.domain.Model.NumberData;
import com.webinfotech.tambolanumbers.domain.Model.SlotNo;
import com.webinfotech.tambolanumbers.domain.executors.impl.ThreadExecutor;
import com.webinfotech.tambolanumbers.presentation.presenters.GetSlotNoPresenter;
import com.webinfotech.tambolanumbers.presentation.presenters.impl.GetSlotNoPresenterImpl;
import com.webinfotech.tambolanumbers.presentation.ui.adapters.NumberListAdapters;
import com.webinfotech.tambolanumbers.presentation.ui.adapters.Tambola_new_game_reycler;
import com.webinfotech.tambolanumbers.presentation.ui.adapters.Tambola_start_recyclerView;
import com.webinfotech.tambolanumbers.presentation.ui.dialogs.NumberListDialog;
import com.webinfotech.tambolanumbers.threading.MainThreadImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Random;

public class StartNewGameActivity extends AppCompatActivity implements GetSlotNoPresenter.View {

    Tambola_new_game_reycler tambola_new_recyclerViewAdapter;
    List<NumberData> tambola_starts_arrayList = new ArrayList<>();
    @BindView(R.id.tick_seek_bar)
    TickSeekBar tickSeekBar;
    @BindView(R.id.board_switch)
    Switch boardSwitch;
    @BindView(R.id.manual_switch)
    Switch manualSwitch;
    @BindView(R.id.sound_switch)
    Switch soundSwitch;
    @BindView(R.id.txt_view_last_number_prv)
    TextView txtViewLastNoPrev;
    @BindView(R.id.txt_view_last_number)
    TextView txtViewLastNumber;
    @BindView(R.id.txt_view_list)
    TextView txtViewList;
    @BindView(R.id.txt_view_current_score)
    TextView txtViewCurrentScore;
    @BindView(R.id.txt_view_touch_play)
    TextView txtViewTouchPlay;
    @BindView(R.id.txt_view_blinking_resume_text)
    TextView txtViewBlinkingText;
    @BindView(R.id.txt_view_delay_time)
    TextView txtViewDelayTime;
    @BindView(R.id.layout_big_score)
    View layoutBigScore;
    @BindView(R.id.txt_view_current_score_big)
    TextView txtViewCurrentScoreBig;
    @BindView(R.id.txt_view_big_status)
    TextView txtViewBigStatus;
    @BindView(R.id.tambula_number_ryc_new)
    RecyclerView recyclerView;
    boolean isManual = true;
    Animation animation;
    int currentNumber = 0;
    List<Integer> previousIntegerArraylist = new ArrayList<>();
    Handler handler = new Handler();
    Runnable runnable;
    int interval = 4000;
    NumberListDialog numberListDialog;
    NumberListAdapters numberListAdapters;
    boolean speakerSound=true;
    TextToSpeech textToSpeech;
    AlertDialog.Builder builder;
    GetSlotNoPresenterImpl mPresenter;
    SlotNo[] slotNos;
    int slotCount = 1;
    private InterstitialAd mInterstitialAd;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_new_game);
        isManual = getIntent().getBooleanExtra("isManual", true);
        ButterKnife.bind(this);
        builder=new AlertDialog.Builder(this);
        initialiseNumberListDialog();
        checkManualAutomatic();
        setArrayList();
        setTickSeekBar();
        setRecyclerView();
        setTexttoSpeech();
        setSwitchListeners();
        initialisePresenter();
        mPresenter.fetchSlotNos();

        // prepare interstitial ads

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-3940256099942544/1033173712");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());

    }

    public void initialisePresenter() {
        mPresenter = new GetSlotNoPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this);
    }



    private void setTexttoSpeech() {
        textToSpeech=new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int i) {
                if (i!=TextToSpeech.ERROR)
                {
                    textToSpeech.setLanguage(Locale.UK);
                }
            }
        });
    }

    private void checkManualAutomatic() {
        if (isManual) {
            txtViewTouchPlay.setVisibility(View.VISIBLE);
            txtViewTouchPlay.setText("Touch to Pick Next Number");
        } else {
            animation= AnimationUtils.loadAnimation(this,R.anim.blink_animation);
            txtViewBlinkingText.startAnimation(animation);
            txtViewBlinkingText.setVisibility(View.VISIBLE);
            txtViewTouchPlay.setVisibility(View.VISIBLE);
            txtViewTouchPlay.setText("Touch to Play");
            manualSwitch.setChecked(true);
            manualSwitch.setText("Automatic");
        }
    }

    private void setRecyclerView() {
        recyclerView=findViewById(R.id.tambula_number_ryc_new);
        tambola_new_recyclerViewAdapter=new Tambola_new_game_reycler(this,tambola_starts_arrayList);
        recyclerView.setLayoutManager(new GridLayoutManager(this,10));
        recyclerView.setAdapter(tambola_new_recyclerViewAdapter);
    }

    private void setArrayList()
    {
        tambola_starts_arrayList=new ArrayList<>();
        for (int i=1;i<=90;i++)
        {
            tambola_starts_arrayList.add(new NumberData(i, false));

        }
    }

    private void setTickSeekBar() {
        tickSeekBar.setOnSeekChangeListener(new OnSeekChangeListener() {
            @Override
            public void onSeeking(SeekParams seekParams) {
            }

            @Override
            public void onStartTrackingTouch(TickSeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(TickSeekBar seekBar) {
                interval = seekBar.getProgress()*1000;
                txtViewDelayTime.setText("Delay:   " + seekBar.getProgress() + "sec");
            }
        });
    }

    public void initialiseNumberListDialog() {
        numberListDialog = new NumberListDialog(this, this);
        numberListDialog.setUpDialogView();
        numberListAdapters = new NumberListAdapters(previousIntegerArraylist);
        numberListDialog.setRecyclerView(numberListAdapters);
    }

    private void setSwitchListeners() {
        boardSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    recyclerView.setVisibility(View.VISIBLE);
                    txtViewTouchPlay.setVisibility(View.VISIBLE);
                    layoutBigScore.setVisibility(View.GONE);
                    txtViewCurrentScore.setVisibility(View.VISIBLE);
                    if (isManual) {
                    } else {
                        if (runnable == null) {

                        } else {

                        }
                    }
                } else {
                    recyclerView.setVisibility(View.GONE);
                    txtViewTouchPlay.setVisibility(View.GONE);
                    layoutBigScore.setVisibility(View.VISIBLE);
                    txtViewCurrentScore.setVisibility(View.GONE);

                    if (isManual) {
                        txtViewBigStatus.setText("Touch to Pick Next Number");
                        txtViewBigStatus.setTextColor(getResources().getColor(R.color.dark_pink));
                    } else {
                        if (runnable == null) {
                            txtViewBigStatus.setText("Touch to Play");
                            txtViewBigStatus.setTextColor(getResources().getColor(R.color.dark_pink));
                        } else {
                            txtViewDelayTime.setVisibility(View.VISIBLE);
                            tickSeekBar.setVisibility(View.VISIBLE);
                            txtViewBigStatus.setText("Touch to Pause(Auto Timer is Running)");
                            txtViewBigStatus.setTextColor(getResources().getColor(R.color.md_grey_700));
                        }
                    }
                }
            }
        });
        soundSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    speakerSound = true;
                } else {
                    speakerSound = false;
                }
            }
        });
        manualSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    isManual = false;
                    animation= AnimationUtils.loadAnimation(getApplicationContext(),R.anim.blink_animation);
                    txtViewBlinkingText.startAnimation(animation);
                    txtViewBlinkingText.setVisibility(View.VISIBLE);
                    txtViewTouchPlay.setTextColor(getResources().getColor(R.color.dark_pink));
                    txtViewTouchPlay.setText("Touch to Play");
                    txtViewBigStatus.setText("Touch to Start");
                    txtViewBigStatus.setTextColor(getResources().getColor(R.color.dark_pink));
                    manualSwitch.setText("Automatic");

                } else {
                    handler.removeCallbacks(runnable);
                    runnable = null;
                    isManual = true;
                    txtViewBlinkingText.clearAnimation();
                    txtViewBlinkingText.setVisibility(View.INVISIBLE);
                    txtViewTouchPlay.setTextColor(getResources().getColor(R.color.dark_pink));
                    txtViewTouchPlay.setText("Touch to Pick Next Number");
                    tickSeekBar.setVisibility(View.INVISIBLE);
                    txtViewDelayTime.setVisibility(View.INVISIBLE);
                    txtViewBigStatus.setText("Touch to Pick Next Number");
                    txtViewBigStatus.setTextColor(getResources().getColor(R.color.dark_pink));
                    manualSwitch.setText("Manual");
                }
            }
        });
    }

    @OnClick(R.id.txt_view_current_score) void onCurrentScoreClicked() {
        txtViewLastNoPrev.setVisibility(View.VISIBLE);
        txtViewLastNumber.setVisibility(View.VISIBLE);
        txtViewList.setVisibility(View.VISIBLE);
        if (isManual) {
            setNextNumber();
        } else {
            if (runnable == null) {
                tickSeekBar.setVisibility(View.VISIBLE);
                txtViewDelayTime.setVisibility(View.VISIBLE);
                txtViewDelayTime.setText("Delay:   4sec");
                txtViewTouchPlay.setTextColor(getResources().getColor(R.color.md_grey_700));
                txtViewTouchPlay.setText("Touch to Pause(Auto Timer is Running)");
                txtViewBlinkingText.setVisibility(View.INVISIBLE);
                txtViewBlinkingText.clearAnimation();
                runnable = new Runnable() {
                    @Override
                    public void run() {
                        setNextNumber();
                        handler.postDelayed(runnable, interval);
                    }
                };
                handler.post(runnable);
            } else {
                handler.removeCallbacks(runnable);
                runnable = null;
                tickSeekBar.setVisibility(View.GONE);
                txtViewDelayTime.setVisibility(View.GONE);
                txtViewTouchPlay.setTextColor(getResources().getColor(R.color.dark_pink));
                txtViewTouchPlay.setText("Touch to Play");
                animation= AnimationUtils.loadAnimation(this,R.anim.blink_animation);
                txtViewBlinkingText.startAnimation(animation);
                txtViewBlinkingText.setVisibility(View.VISIBLE);
            }

        }
    }

    @OnClick(R.id.txt_view_current_score_big) void onCurrentBigScoreClicked() {
        if (isManual) {
            setNextNumber();
        } else {
            if (runnable == null) {
                txtViewDelayTime.setVisibility(View.VISIBLE);
                tickSeekBar.setVisibility(View.VISIBLE);
                txtViewBigStatus.setTextColor(getResources().getColor(R.color.md_grey_700));
                txtViewBigStatus.setText("Touch to Pause(Auto Timer is Running)");
                txtViewBlinkingText.setVisibility(View.INVISIBLE);
                txtViewBlinkingText.clearAnimation();
                runnable = new Runnable() {
                    @Override
                    public void run() {
                        setNextNumber();
                        handler.postDelayed(runnable, interval);
                    }
                };
                handler.post(runnable);
            } else {
                handler.removeCallbacks(runnable);
                runnable = null;
                txtViewDelayTime.setVisibility(View.GONE);
                tickSeekBar.setVisibility(View.INVISIBLE);
                txtViewBigStatus.setTextColor(getResources().getColor(R.color.dark_pink));
                txtViewBigStatus.setText("Touch to Play");
                animation= AnimationUtils.loadAnimation(this,R.anim.blink_animation);
                txtViewBlinkingText.startAnimation(animation);
                txtViewBlinkingText.setVisibility(View.VISIBLE);
            }
        }
        if (slotCount == 90) {

        } else {
            slotCount++;
        }
    }

    @OnClick(R.id.txt_view_list) void onTxtViewListClicked() {
        numberListDialog.showDialog();
        onPause();
    }

    public void setNextNumber() {
        if (previousIntegerArraylist.size() < 90) {
            setNumber();
            updateRecyclerView();
        }
    }
    void setNumber() {
        if (slotCount == 90) {
            handler.removeCallbacks(runnable);
            runnable = null;
            showEndGameDialog();
        } else {
            if (currentNumber > 0) {
                txtViewLastNumber.setText(Integer.toString(currentNumber));
            }
            if (checkCurrentSlotNo(slotCount)) {
                currentNumber = getSlotNoItem(slotCount);
                previousIntegerArraylist.add(currentNumber);
                numberListAdapters.setNewData(previousIntegerArraylist);
                txtViewCurrentScore.setText(Integer.toString(currentNumber));
                txtViewCurrentScoreBig.setText(Integer.toString(currentNumber));
                if (speakerSound)
                {
                    textToSpeech.speak(Integer.toString(currentNumber),TextToSpeech.QUEUE_FLUSH,null);
                }
                slotCount++;
            } else {
                Random random=new Random();
                int number = random.nextInt(90)+1;
                if (checkSameNumber(number) || checkSlotNo(number)) {
                    setNumber();
                } else {
                    currentNumber = number;
                    previousIntegerArraylist.add(currentNumber);
                    numberListAdapters.setNewData(previousIntegerArraylist);
                    if (currentNumber < 10) {
                        txtViewCurrentScore.setText("0" + currentNumber);
                        txtViewCurrentScoreBig.setText("0" + currentNumber);
                    } else {
                        txtViewCurrentScore.setText(Integer.toString(currentNumber));
                        txtViewCurrentScoreBig.setText(Integer.toString(currentNumber));                }
                    if (speakerSound)
                    {
                        speakNumber();
                    }
                    slotCount++;
                }
            }
        }

    }

    void updateRecyclerView() {
        for (int i = 0; i < tambola_starts_arrayList.size(); i++) {
            if (tambola_starts_arrayList.get(i).number == currentNumber) {
                tambola_starts_arrayList.get(i).isSelected = true;
                tambola_new_recyclerViewAdapter.updateDataSet(tambola_starts_arrayList);
                break;
            }
        }
    }

    boolean checkSameNumber(int number) {
        if (previousIntegerArraylist.contains(number)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onBackPressed() {
        builder.setMessage("Do you want to leave the game?");
        builder.setNegativeButton("CONTINUE GAME", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (mInterstitialAd.isLoaded())
                {
                    mInterstitialAd.show();
                    finish();
                }
                else {
                    mInterstitialAd.show();
                    finish();
                }
//                startActivity(new Intent(StartNewGameActivity.this,MainActivity.class));
            }
        });
        builder.show();
    }

    @Override
    public void loadSlotNos(SlotNo[] slotNos) {
        Log.e("LogMsg", "Slot Nos: " + slotNos.length);
        this.slotNos = slotNos;
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (!isManual) {
            handler.removeCallbacks(runnable);
            runnable = null;
            tickSeekBar.setVisibility(View.GONE);
            txtViewDelayTime.setVisibility(View.GONE);
            txtViewTouchPlay.setTextColor(getResources().getColor(R.color.dark_pink));
            txtViewTouchPlay.setText("Touch to Play");
            animation= AnimationUtils.loadAnimation(this,R.anim.blink_animation);
            txtViewBlinkingText.startAnimation(animation);
            txtViewBlinkingText.setVisibility(View.VISIBLE);
            txtViewBigStatus.setTextColor(getResources().getColor(R.color.dark_pink));
            txtViewBigStatus.setText("Touch to Play");
        }
    }

    public boolean checkSlotNo(int number) {
        try {
            for (int i = 0; i < slotNos.length; i++) {
                if (slotNos[i].number == number) {
                    return true;
                }
            }
            return false;
        } catch (NullPointerException e) {
            return false;
        }

    }

    public boolean checkCurrentSlotNo(int slotNo) {
        try {
            for (int i = 0; i < slotNos.length; i++) {
                if (slotNos[i].slotNo == slotNo) {
                    return true;
                }
            }
            return false;
        } catch (NullPointerException e) {
            return false;
        }
    }

    public int getSlotNoItem(int slotNo) {
        for (int i = 0; i < slotNos.length; i++) {
            if (slotNos[i].slotNo == slotNo) {
                return slotNos[i].number;
            }
        }
        return 0;
    }

    public void showEndGameDialog() {
        builder.setMessage("Hope you Enjoy Playing the Game");
        builder.setNegativeButton("GO BACK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        builder.setPositiveButton("RATE US!", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        builder.show();
    }

    public void speakNumber() {
        textToSpeech.speak("",TextToSpeech.QUEUE_FLUSH,null);
        if (currentNumber < 10) {
            textToSpeech.speak("Only Number",TextToSpeech.QUEUE_ADD,null);
            textToSpeech.speak(Integer.toString(currentNumber),TextToSpeech.QUEUE_ADD,null);
        } else {
            String[] numberString = Integer.toString(currentNumber).split("");
            textToSpeech.speak(numberString[1],TextToSpeech.QUEUE_ADD,null);
            textToSpeech.speak(numberString[2],TextToSpeech.QUEUE_ADD,null);
            textToSpeech.speak(Integer.toString(currentNumber),TextToSpeech.QUEUE_ADD,null);
        }
    }

}
