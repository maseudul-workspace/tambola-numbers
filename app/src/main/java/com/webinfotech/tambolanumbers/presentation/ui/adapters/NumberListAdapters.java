package com.webinfotech.tambolanumbers.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.webinfotech.tambolanumbers.R;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class NumberListAdapters extends RecyclerView.Adapter<NumberListAdapters.ViewHolder>{

    List<Integer> numberList;

    public NumberListAdapters(List<Integer> numberList) {
        this.numberList = numberList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_number_list, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewNumberList.setText(Integer.toString(numberList.get(position)));
    }

    @Override
    public int getItemCount() {
        return numberList.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_view_number_list)
        TextView txtViewNumberList;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

    public void setNewData(List<Integer> numberList) {
        this.numberList = numberList;
        notifyDataSetChanged();
    }

}
