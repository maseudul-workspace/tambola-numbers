package com.webinfotech.tambolanumbers.presentation.ui.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.SearchManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.webinfotech.tambolanumbers.BuildConfig;
import com.webinfotech.tambolanumbers.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.txt_view_start)
    TextView txt_view_start;
    @BindView(R.id.txt_view_start_new_board)
    TextView txt_view_StartNew;
    @BindView(R.id.txt_view_settings)
    TextView txt_view_Settings;
    @BindView(R.id.txt_view_download_tambola_ticket)
    TextView txt_view_downloadTambola;
    @BindView(R.id.txt_view_scan_and_play_tambola)
    TextView txt_view_ScanPlay;
    @BindView(R.id.txt_view_how_to_play)
    TextView txt_view_HowToPlay;
    @BindView(R.id.txt_view_best_phones)
    TextView txt_view_PlayTicTak;
    @BindView(R.id.txt_view_share)
    TextView txt_view_Share;
    @BindView(R.id.txt_view_contact_us)
    TextView txt_view_ContactUs;
    @BindView(R.id.txt_view_more_apps)
    TextView txt_view_MoreApps;
    @BindView(R.id.txt_view_privary_policy)
    TextView txt_view_Privacy;
    @BindView(R.id.no_ads)
    ImageView noAdsImg;
//    @BindView(R.id.ads)
//    ImageView AdsImg;
    AlertDialog.Builder builder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        builder=new AlertDialog.Builder(this);

        AdView adView = new AdView(this);
        adView.setAdSize(AdSize.BANNER);
        adView.setAdUnitId("ca-app-pub-3940256099942544/6300978111");
        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });
        adView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);



    }
    @OnClick(R.id.txt_view_best_phones) void bestPhones()
    {
        Intent intent=new Intent(Intent.ACTION_WEB_SEARCH);
        String term="https://techmamu.com/best-5g-phone-to-buy/";
        intent.putExtra(SearchManager.QUERY,term);
        startActivity(intent);
    }

//    @OnClick(R.id.ads)void showAds()
//    {
//        builder.setMessage("Remove Ads!!!");
//        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialogInterface, int i) {
//                dialogInterface.dismiss();
//            }
//        });
//        builder.setPositiveButton("GO PRO!!", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialogInterface, int i) {
//                Toast.makeText(MainActivity.this, "Move to play store paid tambula number", Toast.LENGTH_SHORT).show();
//            }
//        });
//        builder.show();
//    }
    @OnClick(R.id.no_ads) void showNoAds()
    {
        builder.setMessage("Remove Ads!!!");
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.setPositiveButton("GO PRO!!", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(MainActivity.this, "Move to play store paid tambula number", Toast.LENGTH_SHORT).show();
            }
        });
        builder.show();
    }

    @OnClick(R.id.txt_view_start) void startGame()
    {
        Intent intent = new Intent(this, StartGameActivity.class);
        builder.setMessage("Select the mode of Playing");
        builder.setNegativeButton("MANUAL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                intent.putExtra("isManual", true);
                startActivity(intent);
            }
        });
        builder.setPositiveButton("AUTOMATIC", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                intent.putExtra("isManual", false);
                startActivity(intent);
            }
        });
        builder.show();
    }

    @OnClick(R.id.txt_view_start_new_board)void startNewGame()
    {
        Intent intent = new Intent(this, StartNewGameActivity.class);
        builder.setMessage("Select the mode of Playing");
        builder.setPositiveButton("AUTOMATIC", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                intent.putExtra("isManual", false);
                startActivity(intent);            }
        });
        builder.setNegativeButton("MANUAL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                intent.putExtra("isManual", true);
                startActivity(intent);            }
        });
        builder.show();
    }

    @OnClick(R.id.txt_view_settings)void getSetting()
    {
        startActivity(new Intent(MainActivity.this,SettingActivity.class));
    }

    @OnClick(R.id.txt_view_scan_and_play_tambola)void scanPlayTambola()
    {
        builder.setMessage("You need to have Scan and Play tambola ticket in order to use this functionality");
        builder.setNegativeButton("SCAN QR CODE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(MainActivity.this, "scan clicked", Toast.LENGTH_SHORT).show();
            }
        });
        builder.setPositiveButton("DOWNLOAD NOW", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(MainActivity.this, "download clicked", Toast.LENGTH_SHORT).show();
            }
        });
        builder.show();
    }

    @OnClick(R.id.txt_view_how_to_play)void howToPlay()
    {
        builder.setMessage("Learn How to Play Tambola. "+"\n"+" A Youtube video will be played on Clicking Continue");
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(MainActivity.this, "cancel clicked", Toast.LENGTH_SHORT).show();
            }
        });
        builder.setPositiveButton("CONTINUE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(MainActivity.this, "continue clicked", Toast.LENGTH_SHORT).show();
            }
        });
        builder.show();
    }

    @OnClick(R.id.txt_view_share)void sharingFile()
    {
        Intent shareIntent=new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_SUBJECT,"Tambola Numbers");
        String shareMessage="Hi,Try this App\n";
        shareMessage=shareMessage + "https://play.google.com/store/apps/details?id=www.webinfotech.tambolanumbers";
        shareIntent.putExtra(Intent.EXTRA_TEXT,shareMessage);
        startActivity(Intent.createChooser(shareIntent,"Choose any"));
    }


    @OnClick(R.id.txt_view_contact_us)void contactusfunc()
    {
        Intent intent=new Intent(Intent.ACTION_WEB_SEARCH);
        String term="https://www.webinfotech.net.in/";
        intent.putExtra(SearchManager.QUERY,term);
        startActivity(intent);
    }
    @OnClick(R.id.txt_view_download_tambola_ticket)void dowdoadingTambolaTicket()
    {
        Intent intent=new Intent(Intent.ACTION_WEB_SEARCH);
        String term="http://www.amazongiftstore.in/TambolaTickets/";
        intent.putExtra(SearchManager.QUERY,term);
        startActivity(intent);
    }
    @OnClick(R.id.txt_view_privary_policy)void privacyPolicy()
    {
        Intent intent=new Intent(Intent.ACTION_WEB_SEARCH);
        String term="https://www.webinfotech.net.in/";
        intent.putExtra(SearchManager.QUERY,term);
        startActivity(intent);
    }
}

