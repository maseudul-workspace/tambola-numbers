package com.webinfotech.tambolanumbers.repository;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;

public interface AppRepository {

    @GET("numbers/list")
    Call<ResponseBody> getSlotNos();

}
