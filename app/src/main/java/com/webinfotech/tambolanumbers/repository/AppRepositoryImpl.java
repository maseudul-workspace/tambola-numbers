package com.webinfotech.tambolanumbers.repository;

import com.google.gson.Gson;
import com.webinfotech.tambolanumbers.domain.Model.SlotNoWrapper;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class AppRepositoryImpl {

    AppRepository mRepository;

    public AppRepositoryImpl() {
        mRepository = APIclient.createService(AppRepository.class);
    }

    public SlotNoWrapper fetchSlotNoWrapper() {
        SlotNoWrapper slotNoWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> get = mRepository.getSlotNos();

            Response<ResponseBody> response = get.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    slotNoWrapper = null;
                }else{
                    slotNoWrapper = gson.fromJson(responseBody, SlotNoWrapper.class);
                }
            } else {
                slotNoWrapper = null;
            }
        }catch (Exception e){
            slotNoWrapper = null;
        }
        return slotNoWrapper;
    }

}
